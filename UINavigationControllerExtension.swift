//
//  UINavigationControllerExtension.swift
//  Godmode
//
//  Created by Adrian Barbos on 29/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

extension UINavigationController {
    
    internal func pushSignInVC(animated: Bool = true) {
        let story = UIStoryboard(name: "SignIn", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("SignInVC") as! SignInVC
        self.pushViewController(destination, animated: animated)
    }
    
    internal func pushSignUpVC(animated: Bool = true) {
        let story = UIStoryboard(name: "SignUp", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("SignUpVC") as! SignUpVC
        self.pushViewController(destination, animated: animated)
    }
    
    internal func pushFavoritePublishersVC(sender: PublisherListVC, animated: Bool = true) {
        let story = UIStoryboard(name: "FavoritePublishers", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("FavoritePublishersVC") as! FavoritePublishersVC
        destination.delegate = sender
        self.pushViewController(destination, animated: animated)
    }
    
    internal func pushSearchPublishersVC(sender: PublisherListVC, animated: Bool = true) {
        let story = UIStoryboard(name: "SearchPublishers", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("SearchPublishersVC") as! SearchPublishersVC
        destination.delegate = sender
        self.pushViewController(destination, animated: animated)
    }
    
    internal func pushPublisherDetailVC(sender: (SearchPublishersVC?, FavoritePublishersVC?), publisher: Publisher?, animated: Bool = true) {
        let story = UIStoryboard(name: "PublisherDetail", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("PublisherDetailVC") as! PublisherDetailVC
        destination.publisher = publisher
        destination.delegate = sender.0 ?? sender.1
        self.pushViewController(destination, animated: animated)
    }
    
    internal func pushBannerListVC(publisher: Publisher?, animated: Bool = true) {
        let story = UIStoryboard(name: "BannerList", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("BannerListVC") as! BannerListVC
        destination.publisher = publisher
        self.pushViewController(destination, animated: animated)
    }
    
    internal func pushBannerTemplatesVC(banner: Banner?, animated: Bool = true) {
        let story = UIStoryboard(name: "BannerTemplates", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("BannerTemplatesVC") as! BannerTemplatesVC
        destination.banner = banner
        self.pushViewController(destination, animated: animated)
    }
    
}

var SHOULD_AUTOROTATE = true

extension UINavigationController {
    public override func shouldAutorotate() -> Bool {
        return SHOULD_AUTOROTATE
    }
    
    public override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if visibleViewController is BannerPhotoEditVC
            || visibleViewController is Template1VC
            || visibleViewController is Template2VC
            || visibleViewController is Template3VC
            || visibleViewController is Template4VC
            || visibleViewController is Template5VC
            || visibleViewController is Template6VC
        {
            return UIInterfaceOrientationMask.LandscapeRight
        }
        return UIInterfaceOrientationMask.Portrait
    }
    
    
}