//
//  PublisherAPI.swift
//  Godmode
//
//  Created by Adrian Barbos on 30/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation

extension APIClient {
    
    func publishers(page: Int, search: String? = nil, favorite: Bool = false, completion: APICompletionBlock) {
        
        let serverPath = "/publisher\(favorite ? "/favorite" : "")?page=\(page)"
        
        var parameters = [String:AnyObject]()
        if let search = search { parameters["search"] = search }
        
        self.dataTask(
            search != nil ? .POST : .GET,
            serverPath: serverPath,
            params: parameters,
            headers: nil,
            completion: completion
        )
        
    }
    
    func favoritePublisher(publisherId: Int?, completion: APICompletionBlock) {
        
        let serverPath = "/publisher/\(publisherId ?? 0)/favorite"
        
        self.dataTask(
            .POST,
            serverPath: serverPath,
            params: nil,
            headers: nil,
            completion: completion
        )
    }
    
    func unfavoritePublisher(publisherId: Int?, completion: APICompletionBlock) {
        
        let serverPath = "/publisher/\(publisherId ?? 0)/favorite"
        
        self.dataTask(
            .DELETE,
            serverPath: serverPath,
            params: nil,
            headers: nil,
            completion: completion
        )
    }
    
}