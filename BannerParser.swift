//
//  BannerParser.swift
//  Godmode
//
//  Created by Adrian Barbos on 31/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation
import LBJSON

typealias BannersCompletionBlock = (banners: [Banner]?, currentPage: Int?, hasMorePages: Bool?, error: NSError?) -> ()

class BannerParser {
    
    private init() {}
    static let sharedInstance = BannerParser()
    
    internal func parseBanners(response: AnyObject?, completion: BannersCompletionBlock) {
        if let json = LBJSON(object: response),
            let bannerData = json["payload"]?.dictionary?["publishers"]?.array,
            let currentPage = json["metadata"]?.dictionary?["pagination"]?.dictionary?["currentPage"]?.int,
            let hasMorePages = json["metadata"]?.dictionary?["pagination"]?.dictionary?["hasMorePages"]?.bool
        {
            
            var banners = [Banner]()
            for item in bannerData {
                banners.append(Banner(item: item))
            }
            
            completion(banners: banners, currentPage: currentPage, hasMorePages: hasMorePages, error: nil)
            
        } else {
            let error = NSError(domain: "BannerParserDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Can't parse "])
            completion(banners: nil, currentPage: nil, hasMorePages: nil, error: error)
        }
    }
    
}
