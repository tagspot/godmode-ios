//
//  Main.swift
//  Godmode
//
//  Created by Adrian Barbos on 29/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

class Main: UIViewController {

    var loginVC: LoginVC {
        let story = UIStoryboard(name: "Login", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("LoginVC") as! LoginVC
        return destination
    }
    
    var publishersVC: PublisherListVC {
        let story = UIStoryboard(name: "PublisherList", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("PublisherListVC") as! PublisherListVC
        return destination
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkIfLoggedIn()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.login), name: "mainLogin", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.logout), name: "mainLogout", object: nil)
    }

    private func checkIfLoggedIn() {
        self.activeViewController = User.sharedInstance.isLoggedIn ? self.publishersVC : self.loginVC
    }
    
    @objc private func login() {
        self.activeViewController = self.publishersVC
    }
    
    @objc private func logout() {
        self.activeViewController = self.loginVC
    }
    
    //SWAP VC
    private var activeViewController: UIViewController? {
        didSet {
            removeInactiveViewController(oldValue)
            updateActiveViewController()
        }
    }
    
    private func removeInactiveViewController(inactiveViewController: UIViewController?) {
        if let inActiveVC = inactiveViewController {
            // call before removing child view controller's view from hierarchy
            inActiveVC.willMoveToParentViewController(nil)
            
            inActiveVC.view.removeFromSuperview()
            
            // call after removing child view controller's view from hierarchy
            inActiveVC.removeFromParentViewController()
        }
    }
    
    private func updateActiveViewController() {
        if let activeVC = activeViewController {
            // call before adding child view controller's view as subview
            addChildViewController(activeVC)
            
            activeVC.view.frame = view.bounds
            view.addSubview(activeVC.view)
            
            // call before adding child view controller's view as subview
            activeVC.didMoveToParentViewController(self)
        }
    }

}
