//
//  PublisherParser.swift
//  Godmode
//
//  Created by Adrian Barbos on 30/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation
import LBJSON

typealias PublishersCompletionBlock = (publishers: [Publisher]?, currentPage: Int?, hasMorePages: Bool?, error: NSError?) -> ()

class PublisherParser {
    
    private init() {}
    static let sharedInstance = PublisherParser()
    
    internal func parsePublishers(response: AnyObject?, completion: PublishersCompletionBlock) {
        if let json = LBJSON(object: response),
            let publisherData = json["payload"]?.dictionary?["publishers"]?.array,
            let currentPage = json["metadata"]?.dictionary?["pagination"]?.dictionary?["currentPage"]?.int,
            let hasMorePages = json["metadata"]?.dictionary?["pagination"]?.dictionary?["hasMorePages"]?.bool
        {
        
            var publishers = [Publisher]()
            for item in publisherData {
                publishers.append(Publisher(item: item))
            }
            
            completion(publishers: publishers, currentPage: currentPage, hasMorePages: hasMorePages, error: nil)
            
        } else {
            let error = NSError(domain: "PublisherParserDomain", code: 0, userInfo: [NSLocalizedDescriptionKey: "Can't parse "])
            completion(publishers: nil, currentPage: nil, hasMorePages: nil, error: error)
        }
    }
    
}