//
//  StringExtension.swift
//  Godmode
//
//  Created by Adrian Barbos on 29/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

extension String {
    
    init(filename: String, type: String, bundle: NSBundle = NSBundle.mainBundle()) {
        let fileLocation = bundle.pathForResource("terms-and-conditions", ofType: "txt")!
        let content : String
        do
        {
            content = try String(contentsOfFile: fileLocation)
        }
        catch
        {
            content = ""
        }

        self = content
    }
    
    var isNotEmpty: Bool {
        return self.stringByReplacingOccurrencesOfString(" ", withString: "").characters.count > 0
    }
    
    func hasLenghtOf(lenght: Int) -> Bool {
        return self.stringByReplacingOccurrencesOfString(" ", withString: "").characters.count >= lenght
    }
    
    var isValidEmail: Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(self)
    }
    
}
