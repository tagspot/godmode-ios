//
//  UIViewControllerExtension.swift
//  Godmode
//
//  Created by Adrian Barbos on 12/09/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

extension UIViewController {
    @IBAction func goHome() {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
}
