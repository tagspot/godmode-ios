//
//  NotificationAPI.swift
//  Godmode
//
//  Created by Adrian Barbos on 09/08/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation

extension APIClient {

    func registerForNotifications(params: RegisterForNotificationsRequestParams, completion: APICompletionBlock) {
        
        let serverPath = "/device"
        
        var parameters = [String:AnyObject]()
        parameters["token"] = params.token
        parameters["type"] = params.type
        
        self.dataTask(
            .POST,
            serverPath: serverPath,
            params: parameters,
            headers: nil,
            completion: completion
        )
        
    }
    
}
