//
//  AuthenticationAPI.swift
//  Godmode
//
//  Created by Adrian Barbos on 29/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation

extension APIClient {
    
    internal func signIn(params: SignInRequestParams, completion: APICompletionBlock) {
        
        let serverPath = "/login"
        
        var parameters = [String:AnyObject]()
        parameters["email"] = params.email ?? ""
        parameters["password"] = params.password ?? ""
        
        self.dataTask(
            .POST,
            serverPath: serverPath,
            params: parameters,
            headers: nil,
            completion: completion
        )
        
    }
    
    internal func signUp(params: SignUpRequestParams, completion: APICompletionBlock) {
        
        let serverPath = "/register"
        
        var parameters = [String:AnyObject]()
        parameters["email"] = params.email ?? ""
        parameters["password"] = params.password ?? ""
        
        self.dataTask(
            .POST,
            serverPath: serverPath,
            params: parameters,
            headers: nil,
            completion: completion
        )
        
    }
    
    internal func facebookLogin(token: String?, completion: APICompletionBlock) {
        
        let serverPath = "/login/facebook"
        
        var parameters = [String:AnyObject]()
        parameters["token"] = token ?? ""
        
        self.dataTask(
            .POST,
            serverPath: serverPath,
            params: parameters,
            headers: nil,
            completion: completion
        )
        
    }
    
}