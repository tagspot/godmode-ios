//
//  BannersDataSource.swift
//  Godmode
//
//  Created by Adrian Barbos on 31/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation

class BannersDataSouce {
    
    var banners: [Banner]?
    var currentPage: Int = 1
    var hasMorePages = false
    
    func loadBanners(page: Int = 1, publisherId: Int?, completion: BannersCompletionBlock) {
        self.loadBannersMethod(page, publisherId: publisherId, completion: completion)
    }
    
    func logShared(bannerId: Int?, completion: StatusCompletionBlock) {
        self.logSharedMethod(bannerId, completion: completion)
    }
    
}

extension BannersDataSouce {
    
    func bannerAtIndex(index: Int) -> Banner? {
        if let objects = self.banners {
            if index < 0 || index >= objects.count {
                return nil
            } else {
                return objects[index]
            }
        }
        return nil
    }
    
    func bannersCount() -> Int {
        return self.banners?.count ?? 0
    }
    
}

extension BannersDataSouce {
    
    private func loadBannersMethod(page: Int = 1, publisherId: Int?, completion: BannersCompletionBlock) {
        
        APIClient.shareInstance().banners(page, publisherId: publisherId) { (response, error, statusCode) in
            if error == nil {
                BannerParser.sharedInstance.parseBanners(response, completion: { (banners, currentPage, hasMorePages, error) in
                    if error == nil {
                        self.currentPage = currentPage!
                        self.hasMorePages = hasMorePages!
                        completion(banners: banners, currentPage: currentPage, hasMorePages: hasMorePages, error: nil)
                    } else {
                        completion(banners: nil, currentPage: nil, hasMorePages: nil, error: error)
                    }
                })
            } else {
                completion(banners: nil, currentPage: nil, hasMorePages: nil, error: error)
            }
        }
        
    }
    
    private func logSharedMethod(bannerId: Int?, completion: StatusCompletionBlock) {
        APIClient.shareInstance().logSharedBanner(bannerId) { (response, error, statusCode) in
            completion(error: error)
        }
    }
    
}