//
//  DisclaimerVC.swift
//  Godmode
//
//  Created by Adrian Barbos on 16/09/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

protocol DisclaimerDelegate {
    func disclaimerWasApproved()
    func disclaimerWasRejected()
}

class DisclaimerVC: UIViewController {
    
    @IBOutlet weak var textView: UITextView!
    
    static var approved: Bool {
        get {
            return NSUserDefaults.standardUserDefaults().boolForKey("disclaimerApproved") ?? false
        } set {
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "disclaimerApproved")
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.textView.attributedText = self.getAttrText()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.textView.setContentOffset(CGPoint.zero, animated: false)
        
    }
    
    @IBAction func acceptButtonTapped() {
        DisclaimerVC.approved = true
        self.dismissViewControllerAnimated(false, completion: nil)
    }
    
    @IBAction func refuseButtonTapped() {
        UIApplication.sharedApplication().performSelector(#selector(NSURLSessionTask.suspend))
    }
    
    private func getAttrText() -> NSAttributedString {
        let attr = NSMutableAttributedString()
        
        attr.appendAttributedString(
            NSAttributedString(string: String(filename: "terms-and-conditions", type: "txt"))
        )
        
       return attr
    }

}
