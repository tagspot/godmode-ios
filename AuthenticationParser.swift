//
//  AuthenticationParser.swift
//  Godmode
//
//  Created by Adrian Barbos on 29/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation
import LBJSON

enum SignInStatus {
    case ok
    case wrongPassword
    case networkError
}

enum SignUpStatus {
    case ok
    case emailExists
    case networkError
}

typealias SignInCompletionBlock = (token: String?, status: SignInStatus) -> ()
typealias SignUpCompletionBlock = (token: String?, status: SignUpStatus) -> ()
typealias FacebookLoginCompletionBlock = (token: String?, error: NSError?) -> ()

class AuthenticationParser {
    
    private init() {}
    static let sharedInstance = AuthenticationParser()
    
    internal func parseSignIn(response: AnyObject?, completion: SignInCompletionBlock) {
        if let json = LBJSON(object: response), let token = json["payload"]?.dictionary?["user"]?.dictionary?["api_token"]?.str {
            completion(token: token, status: SignInStatus.ok)
        } else {
            completion(token: nil, status: SignInStatus.networkError)
        }
    }
    
    internal func parseSignUp(response: AnyObject?, completion: SignUpCompletionBlock) {
        if let json = LBJSON(object: response), let token = json["payload"]?.dictionary?["user"]?.dictionary?["api_token"]?.str {
            completion(token: token, status: SignUpStatus.ok)
        } else {
            completion(token: nil, status: SignUpStatus.networkError)
        }
    }
    
    internal func parseFacebookLogin(response: AnyObject?, completion: FacebookLoginCompletionBlock) {
        if let json = LBJSON(object: response), let token = json["payload"]?.dictionary?["user"]?.dictionary?["api_token"]?.str {
            completion(token: token, error: nil)
        } else {
            completion(token: nil, error: NSError(domain: "", code: 0, userInfo: nil))
        }
    }
    
}
