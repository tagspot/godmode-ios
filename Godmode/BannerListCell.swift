//
//  BannerListCell.swift
//  Godmode
//
//  Created by Andrei Calbajos on 28/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import Alamofire

class BannerListCell: UITableViewCell {
    
    weak var model: Banner?
    
    @IBOutlet weak var bannerImageView: UIImageView!
    
    func configureCellWithModel(model: Banner?) {
        self.model = model
        
        bannerImageView.backgroundColor = UIColor.blackColor()
        bannerImageView.image = nil
        if let stringUrl = self.model?.bannerUrl, let url = NSURL(string: stringUrl) {
            bannerImageView.af_setImageWithURL(url, imageTransition: UIImageView.ImageTransition.CrossDissolve(1), runImageTransitionIfCached: false)
        }
        
    }

}
