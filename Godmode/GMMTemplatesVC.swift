//
//  GMMTemplatesVC.swift
//  Godmode
//
//  Created by Andrei Calbajos on 08/08/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

class GMMTemplatesVC: UIViewController {
    
    @IBOutlet weak var tableViewobj: UITableView!
    
    let gmmTemplates : [String] = ["profilbanner1.jpg", "profilbanner2.jpg", "profilbanner4.jpg", "profilbanner5.jpg", "Profilbanner6.jpg", "Profilbanner7.jpg"]
    let gmmTemplatesSlices : [[String]] = [
        ["11.png", "12.png", "13.png", "14.png", "15.png"],
        ["21.png", "22.png", "23.png", "24.png"],
        ["31.png", "32.png", "33.png", "34.png", "35.png", "36.png"],
        ["41.png", "42.png", "43.png", "44.png"],
        ["51.png", "52.png", "53.png"],
        ["61.png", "62.png"]]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 26.0/255.0, green: 27.0/255.0, blue: 31.0/255.0, alpha: 1)
    }
    
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }

}

extension GMMTemplatesVC: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gmmTemplates.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellID = "GMMTemplatesCell"
        let cell = tableViewobj.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as! GMMTemplatesCell
        
        cell.entityImageView.image = UIImage(named: gmmTemplates[indexPath.row])
        
        return cell
    }
}

extension GMMTemplatesVC: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        switch indexPath.row {
            
        case 0:
            
            let story = UIStoryboard(name: "Template1", bundle: nil)
            let destination = story.instantiateViewControllerWithIdentifier("Template1VC") as! Template1VC
            self.navigationController?.pushViewController(destination, animated: true)
            
            break
        case 1:
            
            let story = UIStoryboard(name: "Template2", bundle: nil)
            let destination = story.instantiateViewControllerWithIdentifier("Template2VC") as! Template2VC
            self.navigationController?.pushViewController(destination, animated: true)
            
            break
        case 2:
            
            let story = UIStoryboard(name: "Template3", bundle: nil)
            let destination = story.instantiateViewControllerWithIdentifier("Template3VC") as! Template3VC
            self.navigationController?.pushViewController(destination, animated: true)
            
            break
        case 3:
            
            let story = UIStoryboard(name: "Template4", bundle: nil)
            let destination = story.instantiateViewControllerWithIdentifier("Template4VC") as! Template4VC
            self.navigationController?.pushViewController(destination, animated: true)
            
            break
        case 4:
            
            let story = UIStoryboard(name: "Template5", bundle: nil)
            let destination = story.instantiateViewControllerWithIdentifier("Template5VC") as! Template5VC
            self.navigationController?.pushViewController(destination, animated: true)
            
            break
        case 5:
            
            let story = UIStoryboard(name: "Template6", bundle: nil)
            let destination = story.instantiateViewControllerWithIdentifier("Template6VC") as! Template6VC
            self.navigationController?.pushViewController(destination, animated: true)

            break
            
        default:
            break
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (UIScreen.mainScreen().bounds.width * 315) / 851
    }
}

