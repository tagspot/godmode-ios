//
//  BannerTemplatesCell.swift
//  Godmode
//
//  Created by Andrei Calbajos on 28/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

class BannerTemplatesCell: UITableViewCell {
    
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var templateLeftImageView: UIImageView!
    @IBOutlet weak var templateRightImageView: UIImageView!


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
