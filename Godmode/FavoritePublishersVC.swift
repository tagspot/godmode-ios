//
//  FavoritePublishersVC.swift
//  Godmode
//
//  Created by Adrian Barbos on 30/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll

class FavoritePublishersVC: UIViewController {
    
    var delegate: PublisherDetailDelegate?
    
    let dataSource = PublisherDataSource()
    
    @IBOutlet weak var entitiesTableView: UITableView!
    var refreshControl:UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 26.0/255.0, green: 27.0/255.0, blue: 31.0/255.0, alpha: 1)
        self.addPullToRefresh()
        self.addLazyLoading()
    }
    
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}

extension FavoritePublishersVC: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.publishersCount()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellID = "FavoritePublishersCell"
        let cell = entitiesTableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as! FavoritePublishersCell
        cell.configureCellWithModel(self.dataSource.publisherAtIndex(indexPath.row))
        
        return cell
    }
}

extension FavoritePublishersVC: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.navigationController?.pushPublisherDetailVC((nil, self), publisher: self.dataSource.publisherAtIndex(indexPath.row))
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (UIScreen.mainScreen().bounds.width * 315) / 851
    }
}

extension FavoritePublishersVC: PublisherDetailDelegate {
    func publisherDetail(favoritedOrUnfavorited publisher: Publisher?) {
        self.delegate?.publisherDetail(favoritedOrUnfavorited: publisher)
        
        guard self.dataSource.publishers != nil && publisher != nil else { return }
        
        for (index, value) in self.dataSource.publishers!.enumerate() where value.id == publisher!.id {
            self.dataSource.publishers!.removeAtIndex(index)
            self.entitiesTableView.reloadData()
        }
    }
}


extension FavoritePublishersVC {
    func addPullToRefresh() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = UIColor.blackColor()
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.entitiesTableView.addSubview(refreshControl)
        
        self.entitiesTableView.setContentOffset(CGPointMake(0, -self.refreshControl.frame.size.height), animated: true)
        self.refreshControl.beginRefreshing()
        
        self.getData()
    }
    
    func refresh(sender:AnyObject) {
        self.getData()
    }
    
    private func getData() {
        self.dataSource.loadPublishers (favorite: true, completion: { (publishers, currentPage, hasMorePages, error) in
            if error == nil {
                self.dataSource.publishers = publishers
                self.entitiesTableView.reloadData()
                self.refreshControl.endRefreshing()
            } else {
                self.entitiesTableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        })
    }
}

extension FavoritePublishersVC {
    private func addLazyLoading() {
        self.entitiesTableView.infiniteScrollIndicatorStyle = .Gray
        self.entitiesTableView.addInfiniteScrollWithHandler { (tableView) in
            
            guard self.dataSource.hasMorePages else {
                tableView.finishInfiniteScroll()
                return
            }
            
            let page = self.dataSource.currentPage + 1
            
            self.dataSource.loadPublishers(page, favorite: true, completion: { (publishers, currentPage, hasMorePages, error) in
                if error == nil {
                    var currentCount = self.dataSource.publishersCount() - 1
                    var indexPaths = [NSIndexPath]()
                    for item in publishers! {
                        currentCount += 1
                        let indexPath = NSIndexPath(forRow: currentCount, inSection: 0)
                        
                        indexPaths.append(indexPath)
                        self.dataSource.publishers?.append(item)
                    }
                    
                    tableView.beginUpdates()
                    tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
                    tableView.endUpdates()
                    
                }
                tableView.finishInfiniteScroll()
            })
            
        }
        
    }
}