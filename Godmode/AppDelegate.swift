//
//  AppDelegate.swift
//  Godmode
//
//  Created by Andrei Calbajos on 25/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import LBJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().enableAutoToolbar = false
        
        Fabric.with([Crashlytics.self])
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
        
        if let launchOpts = launchOptions, let userInfo = launchOpts[UIApplicationLaunchOptionsRemoteNotificationKey] as? [NSObject : AnyObject] {
            self.handleReceivedNotification(application, userInfo: userInfo)
        }
        
        return true
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

extension AppDelegate {
    //MARK: - Push Notifications
    
    class func registerForRemoteNotifications() {
        let settings = UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert], categories: nil)
        UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    class func isRegisteredForRemoteNotifications() -> Bool {
        return UIApplication.sharedApplication().isRegisteredForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        var token = String(format: "%@", deviceToken)
        print(deviceToken)
        token = token.stringByReplacingOccurrencesOfString("<", withString: "")
        token = token.stringByReplacingOccurrencesOfString(">", withString: "")
        token = token.stringByReplacingOccurrencesOfString(" ", withString: "")
        
        self.registerDeviceToken(token)
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        self.handleReceivedNotification(application, userInfo: userInfo)
    }
    
    func handleReceivedNotification(application: UIApplication, userInfo: [NSObject : AnyObject]) {
        if let payload = LBJSON(object: userInfo["payload"]), let bannerData = payload["banner"], let aps = LBJSON(object: userInfo["aps"]), let message = aps["alert"]?.str {
            
            let banner = Banner(item: bannerData)
            
            if ( application.applicationState == UIApplicationState.Inactive || application.applicationState == UIApplicationState.Background  )
            {
                (self.window?.rootViewController as? UINavigationController)?.pushBannerTemplatesVC(banner)
            } else {
                
                let alert = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
                alert.addAction(
                    UIAlertAction(title: "Show", style: .Default, handler: { (_) in
                        (self.window?.rootViewController as? UINavigationController)?.pushBannerTemplatesVC(banner)
                    })
                )
                alert.addAction(
                    UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
                )
                self.window?.rootViewController?.presentViewController(alert, animated: true, completion: nil)
                
            }
        }
    }
    
    func registerDeviceToken(token: String) {
        let params = RegisterForNotificationsRequestParams()
        params.token = token
        
        APIClient.shareInstance().registerForNotifications(
            params) { (response, error, statusCode) in
                
        }
    }
}

