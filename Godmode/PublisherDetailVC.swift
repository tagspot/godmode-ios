//
//  EntityDetailVC.swift
//  Godmode
//
//  Created by Andrei Calbajos on 27/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import KVNProgress
import AlamofireImage
import GoogleMobileAds

protocol PublisherDetailDelegate: class {
    func publisherDetail(favoritedOrUnfavorited publisher: Publisher?)
}

class PublisherDetailVC: UIViewController {
    
    let datasource = PublisherDataSource()
    
    var delegate: PublisherDetailDelegate?
    
     var publisher: Publisher?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var coverImageView: UIImageView!
    @IBOutlet weak var detailTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    private func setup() {
        self.detailTableView.rowHeight = UITableViewAutomaticDimension
        self.detailTableView.estimatedRowHeight = 100
        
        self.titleLabel.text = self.publisher?.name ?? ""
        
        self.coverImageView.backgroundColor = UIColor.blackColor()
        self.coverImageView.image = nil
        if let stringUrl = self.publisher?.converUrl, let url = NSURL(string: stringUrl) {
            self.coverImageView.af_setImageWithURL(url, imageTransition: UIImageView.ImageTransition.CrossDissolve(1), runImageTransitionIfCached: false)
        }
    }
    
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func favoriteTap() {
        KVNProgress.show()
        if !self.publisher!.favorite {
            self.datasource.favoritePublisher(self.publisher!.id, completion: { (error) in
                if error == nil {
                    self.publisher!.favorite = true
                    self.detailTableView.reloadData()
                    self.delegate?.publisherDetail(favoritedOrUnfavorited: self.publisher)
                    KVNProgress.dismiss()
                } else {
                    KVNProgress.showError()
                }
            })
        } else {
            self.datasource.unfavoritePublisher(self.publisher!.id, completion: { (error) in
                if error == nil {
                    self.publisher!.favorite = false
                    self.detailTableView.reloadData()
                    self.delegate?.publisherDetail(favoritedOrUnfavorited: self.publisher)
                    KVNProgress.dismiss()
                } else {
                    KVNProgress.showError()
                }
            })
        }
    }
    
    @IBAction func bannersTap() {
        self.navigationController?.pushBannerListVC(self.publisher)
    }
}

extension PublisherDetailVC: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellID = "PublisherDetailCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as! PublisherDetailCell
        cell.backgroundColor = UIColor(red: 26.0/255.0, green: 27.0/255.0, blue: 31.0/255.0, alpha: 1)
        cell.descriptionLabel.text = self.publisher?.desc ?? ""
        cell.favorite = self.publisher?.favorite ?? false
        
        cell.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        cell.bannerView.rootViewController = self
        cell.bannerView.loadRequest(GADRequest())
        
        return cell
        
    }
}
