//
//  BannerTemplatesVC.swift
//  Godmode
//
//  Created by Andrei Calbajos on 28/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

class BannerTemplatesVC: UIViewController {
    
    let templatesArray: [String] = ["1-r.png", "2-r.png", "3-l.png", "4-r.png", "5-l.png", "6-r.png", "7-r.png"]
    
    @IBOutlet weak var tableViewObj: UITableView!
 
    var banner: Banner?

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}

extension BannerTemplatesVC: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cellID = "BannerTemplatesCell"
        let cell = tableViewObj.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as! BannerTemplatesCell
        
        cell.bannerImageView.backgroundColor = UIColor.blackColor()
        cell.bannerImageView.image = nil
        if let stringUrl = self.banner?.bannerUrl, let url = NSURL(string: stringUrl) {
            cell.bannerImageView.af_setImageWithURL(url, imageTransition: UIImageView.ImageTransition.CrossDissolve(1), runImageTransitionIfCached: false)
        }
        
        
        cell.templateLeftImageView.image = UIImage(named: templatesArray[indexPath.row])
        cell.templateRightImageView.image = UIImage(named: templatesArray[indexPath.row])
        
        if indexPath.row == 2 || indexPath.row == 4 {
            cell.templateRightImageView.hidden = true
            cell.templateLeftImageView.hidden = false
            
        } else  {
            
            cell.templateRightImageView.hidden = false
            cell.templateLeftImageView.hidden = true
            
        }
        
        return cell
        
    }
}

extension BannerTemplatesVC: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let story = UIStoryboard(name: "BannerPhotoEdit", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("BannerPhotoEditVC") as! BannerPhotoEditVC
        destination.banner = self.banner
        destination.templateImage = UIImage(named: templatesArray[indexPath.row])
        destination.templatePosition = indexPath.row == 2 || indexPath.row == 4 ? .left : .right
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (UIScreen.mainScreen().bounds.width * 315) / 851
    }
}
