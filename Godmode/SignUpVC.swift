//
//  SignUpVC.swift
//  Godmode
//
//  Created by Andrei Calbajos on 26/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import KVNProgress

class SignUpVC: UIViewController, UITextFieldDelegate {
    
    let datasource = AuthenticationDataSource()
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func signUpTap() {
        guard self.validateFields() else { return }
        
        self.view.endEditing(false)
        KVNProgress.show()
        
        let params = SignUpRequestParams()
        params.email = self.emailTextField.text
        params.password = self.passwordTextField.text
        
        self.datasource.register(params) { (token, status) in
            KVNProgress.dismiss()
            switch status {
            case .ok:
                User.sharedInstance.login(self, token: token)
                break
            case .emailExists:
                self.showAlert("Email address is already used by another account")
                break
            case.networkError:
                self.showAlert("An unexpected error occured. Please check your internet connection and try again")
                break
            }
        }
    }
    
    @IBAction func signInTap() {
        self.navigationController?.pushSignInVC()
    }

}

extension SignUpVC {
    private func validateFields() -> Bool {
        guard self.emailTextField.text!.isNotEmpty else {
            self.showAlert("Email field is required")
            return false
        }
        
        guard self.passwordTextField.text!.isNotEmpty else {
            self.showAlert("Password field is required")
            return false
        }
        
        guard self.emailTextField.text!.isValidEmail else {
            self.showAlert("Email must be a valid email address")
            return false
        }
        
        guard self.passwordTextField.text!.hasLenghtOf(6) else {
            self.showAlert("Password must contain at least 6 characters")
            return false
        }
        return true
    }
    
    private func showAlert(message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

extension SignUpVC {
    private func setup() {
        self.styleSignInButton()
        self.styleSignUpButton()
        self.styleViews()
    }
    
    private func styleSignInButton() {
        
        let button = self.signInButton
        button.layer.cornerRadius = 15
        button.layer.borderWidth = 1
        button.layer.backgroundColor = UIColor.clearColor().CGColor
        button.layer.borderColor = UIColor(red: 130.0/255.0, green: 129.0/255.0, blue: 134.0/255.0, alpha: 1).CGColor
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Selected)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Highlighted)
        button.setTitle("Already have an account? Sign In", forState: UIControlState.Normal)
        button.setTitle("Already have an account? Sign In", forState: UIControlState.Selected)
        button.setTitle("Already have an account? Sign In", forState: UIControlState.Highlighted)
        
    }
    
    private func styleSignUpButton() {
        
        let button = self.signUpButton
        button.layer.cornerRadius = 1
        button.layer.backgroundColor = UIColor(red: 207.0/255.0, green: 28.0/255.0, blue: 34.0/255.0, alpha: 1).CGColor
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Selected)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Highlighted)
        button.setTitle("Sign Up", forState: UIControlState.Normal)
        button.setTitle("Sign Up", forState: UIControlState.Selected)
        button.setTitle("SIgn Up", forState: UIControlState.Highlighted)
        
    }
    
    private func styleViews() {
        
        let view1 = self.emailView
        view1.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.1)
        view1.layer.borderWidth = 1
        view1.layer.cornerRadius = 2
        view1.layer.borderColor = UIColor(red: 161.0/255.0, green: 160.0/255.0, blue: 161.0/255.0, alpha: 1).CGColor
        
        let view2 = self.passwordView
        view2.backgroundColor = UIColor.lightGrayColor().colorWithAlphaComponent(0.1)
        view2.layer.borderWidth = 1
        view2.layer.cornerRadius = 2
        view2.layer.borderColor = UIColor(red: 161.0/255.0, green: 160.0/255.0, blue: 161.0/255.0, alpha: 1).CGColor
        
    }
}
