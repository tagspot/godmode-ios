//
//  EntityDetailCell.swift
//  Godmode
//
//  Created by Andrei Calbajos on 27/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import GoogleMobileAds


class PublisherDetailCell: UITableViewCell {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var setFavoriteButton: UIButton!
    @IBOutlet weak var getBannersButton: UIButton!
    @IBOutlet weak var bannerView: GADBannerView!
    var favorite: Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        self.configureCell()
    }
    
    private func configureCell() {
        self.styleSetFavoriteButton()
        self.styleGetBannersButton()
    }
    
    private func styleSetFavoriteButton() {
        
        
        let button = self.setFavoriteButton
        button.layer.cornerRadius = 2
        button.layer.borderWidth = 1
        button.layer.backgroundColor = favorite ? UIColor.whiteColor().CGColor : UIColor(red: 26.0/255.0, green: 27.0/255.0, blue: 31.0/255.0, alpha: 1).CGColor
        button.layer.borderColor = favorite ? UIColor(red: 26.0/255.0, green: 27.0/255.0, blue: 31.0/255.0, alpha: 1).CGColor : UIColor(red: 130.0/255.0, green: 129.0/255.0, blue: 134.0/255.0, alpha: 1).CGColor
        button.setTitleColor(favorite ? UIColor(red: 26.0/255.0, green: 27.0/255.0, blue: 31.0/255.0, alpha: 1) : UIColor(red: 130.0/255.0, green: 129.0/255.0, blue: 134.0/255.0, alpha: 1), forState: UIControlState.Normal)
        button.setTitle(favorite ? "Remove favorite" : "Set favorite", forState: UIControlState.Normal)
        
    }
    
    private func styleGetBannersButton() {
        
        let button = self.getBannersButton
        button.layer.cornerRadius = 1
        button.layer.backgroundColor = UIColor(red: 207.0/255.0, green: 28.0/255.0, blue: 34.0/255.0, alpha: 1).CGColor
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        button.setTitle("Get Banners", forState: UIControlState.Normal)
        
    }

}
