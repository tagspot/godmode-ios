//
//  EntitiesTableViewCell.swift
//  Godmode
//
//  Created by Andrei Calbajos on 25/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import AlamofireImage

class PublisherListCell: UITableViewCell {
    
    weak var model: Publisher?
    
    @IBOutlet weak var entityImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func configureCellWithModel(model: Publisher?) {
        self.model = model
        
        if let name = self.model?.name {
            self.titleLabel.text = name
        }
        
        entityImageView.backgroundColor = UIColor.blackColor()
        entityImageView.image = nil
        if let stringUrl = self.model?.converUrl, let url = NSURL(string: stringUrl) {
            entityImageView.af_setImageWithURL(url, imageTransition: UIImageView.ImageTransition.CrossDissolve(1), runImageTransitionIfCached: false)
        }
        
    }
    
}