//
//  LoginVC.swift
//  Godmode
//
//  Created by Andrei Calbajos on 26/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import KVNProgress

class LoginVC: UIViewController {
    
    var previousPage: UInt = 11
    
    let datasource = AuthenticationDataSource()
    
    @IBOutlet weak var background1IV: UIImageView!
    @IBOutlet weak var background2IV: UIImageView!
    
    @IBOutlet weak var connectFaceBookButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.showDisclaimer()
    }
    
    @IBAction func showDisclaimer() {
        
        guard !DisclaimerVC.approved else { return }
        
        let story = UIStoryboard(name: "Disclaimer", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("DisclaimerVC") as! DisclaimerVC
        destination.modalPresentationStyle = .OverCurrentContext
        self.presentViewController(destination, animated: false, completion: nil)
        
    }
    
    @IBAction func facebookButtonTap() {
        FacebookLoginHelper.sharedInstance.login(self) { (status, token) in
            switch status {
            case .success:
                self.processFacebookLogin(token)
                break
            case .error:
                self.showAlert("An unexpected error occured. Please check your internet connection and try again")
                break
            case .canceled:
                break
            }
        }
    }
    
    private func processFacebookLogin(token: String?) {
        KVNProgress.show()
        self.datasource.facebookLogin(token) { (token, error) in
            KVNProgress.dismiss()
            if error == nil {
                User.sharedInstance.login(self, token: token)
            } else {
                self.showAlert("An unexpected error occured. Please check your internet connection and try again")
            }
        }
    }
    
    @IBAction func signUpButtonTap() {
        self.navigationController?.pushSignUpVC()
    }

    @IBAction func loginButtonTap() {
        self.navigationController?.pushSignInVC()
    }

}

extension LoginVC {
    private func showAlert(message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

extension LoginVC {
    
    private func setup() {
        self.styleSignInButton()
    }
    
    private func styleSignInButton() {
        
        let button = self.signInButton
        button.layer.cornerRadius = 15
        button.layer.borderWidth = 1
        button.layer.backgroundColor = UIColor.clearColor().CGColor
        button.layer.borderColor = UIColor(red: 130.0/255.0, green: 129.0/255.0, blue: 134.0/255.0, alpha: 1).CGColor
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Selected)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Highlighted)
        button.setTitle("Already have an account? Sign In", forState: UIControlState.Normal)
        button.setTitle("Already have an account? Sign In", forState: UIControlState.Selected)
        button.setTitle("Already have an account? Sign In", forState: UIControlState.Highlighted)
    }
    
}
