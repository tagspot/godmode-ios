//
//  SearchPublishersVC.swift
//  Godmode
//
//  Created by Adrian Barbos on 30/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

class SearchPublishersVC: UIViewController {
    
    weak var delegate: PublisherDetailDelegate?
    
    let dataSource = PublisherDataSource()
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var entitiesTableView: UITableView!
    var refreshControl:UIRefreshControl!
    
    var searchQuery = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 26.0/255.0, green: 27.0/255.0, blue: 31.0/255.0, alpha: 1)
        self.addPullToRefresh()
        self.addLazyLoading()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.searchField.becomeFirstResponder()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        if self.searchField.isFirstResponder() {
            self.searchField.resignFirstResponder()
        }
    }
    
    @IBAction func back() {
        self.searchField.resignFirstResponder()
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func search() {
        
        guard self.searchField.text!.isNotEmpty else { return }
        
        self.searchQuery = self.searchField.text!
        
        APIClient.shareInstance().cancelAllRequests()
        
        self.entitiesTableView.setContentOffset(CGPointMake(0, -self.refreshControl.frame.size.height), animated: true)
        self.refreshControl.beginRefreshing()
        self.getData()
    }
}

extension SearchPublishersVC: UITextFieldDelegate {
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.search()
        return false
    }
}

extension SearchPublishersVC: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.publishersCount()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellID = "SearchPublishersCell"
        let cell = entitiesTableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as! SearchPublishersCell
        cell.configureCellWithModel(self.dataSource.publisherAtIndex(indexPath.row))
        
        return cell
    }
}

extension SearchPublishersVC: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.navigationController?.pushPublisherDetailVC((self,nil), publisher: self.dataSource.publisherAtIndex(indexPath.row))
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (UIScreen.mainScreen().bounds.width * 315) / 851
    }
}

extension SearchPublishersVC: PublisherDetailDelegate {
    func publisherDetail(favoritedOrUnfavorited publisher: Publisher?) {
        self.delegate?.publisherDetail(favoritedOrUnfavorited: publisher)
    }
}

extension SearchPublishersVC {
    func addPullToRefresh() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = UIColor.blackColor()
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.entitiesTableView.addSubview(refreshControl)
    }
    
    func refresh(sender:AnyObject) {
        self.refreshControl.endRefreshing()
    }
    
    private func getData() {
        self.dataSource.loadPublishers (search: self.searchQuery, completion: { (publishers, currentPage, hasMorePages, error) in
            if error == nil {
                self.dataSource.publishers = publishers
                self.entitiesTableView.reloadData()
                self.refreshControl.endRefreshing()
            } else {
                self.entitiesTableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        })
    }
}

extension SearchPublishersVC {
    private func addLazyLoading() {
        self.entitiesTableView.infiniteScrollIndicatorStyle = .Gray
        self.entitiesTableView.addInfiniteScrollWithHandler { (tableView) in
            
            guard self.dataSource.hasMorePages else {
                tableView.finishInfiniteScroll()
                return
            }
            
            let page = self.dataSource.currentPage + 1
            
            self.dataSource.loadPublishers(page, search: self.searchQuery, completion: { (publishers, currentPage, hasMorePages, error) in
                if error == nil {
                    var currentCount = self.dataSource.publishersCount() - 1
                    var indexPaths = [NSIndexPath]()
                    for item in publishers! {
                        currentCount += 1
                        let indexPath = NSIndexPath(forRow: currentCount, inSection: 0)
                        
                        indexPaths.append(indexPath)
                        self.dataSource.publishers?.append(item)
                    }
                    
                    tableView.beginUpdates()
                    tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
                    tableView.endUpdates()
                    
                }
                tableView.finishInfiniteScroll()
            })
            
        }
        
    }
}