//
//  Entity.swift
//  Godmode
//
//  Created by Andrei Calbajos on 25/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation
import LBJSON

class Publisher {
    
    var id: Int?
    var name: String?
    var desc: String?
    var converUrl: String?
    var favorite: Bool = false
    
    init(item: LBJSON) {
        self.id = item["id"]?.int
        self.name = item["name"]?.str
        self.desc = item["description"]?.str
        self.converUrl = item["cover_url"]?.str
        self.favorite = item["favorite"]?.bool ?? false
    }
    
}