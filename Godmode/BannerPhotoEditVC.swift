//
//  BannerPhotoEditVC.swift
//  Godmode
//
//  Created by Adrian Barbos on 31/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

enum TemplatePosition {
    case right
    case left
}

class BannerPhotoEditVC: UIViewController {
    
    let datasource = BannersDataSouce()

    weak var banner: Banner?
    var templateImage: UIImage?
    var templatePosition: TemplatePosition = .left
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bannerImageView: UIImageView!
    
    @IBOutlet weak var leftPlaceholder: UIImageView!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var leftScrollView: UIScrollView!
    
    
    @IBOutlet weak var leftBorderView: UIView!
    
    @IBOutlet weak var leftImage: UIImageView!
    @IBOutlet weak var leftImageTop: NSLayoutConstraint!
    @IBOutlet weak var leftImageBottom: NSLayoutConstraint!
    @IBOutlet weak var leftImageRight: NSLayoutConstraint!
    @IBOutlet weak var leftImageLeft: NSLayoutConstraint!
    
    @IBOutlet weak var rightPlaceholder: UIImageView!
    @IBOutlet weak var rightView: UIView!
    @IBOutlet weak var rightScrollView: UIScrollView!
    
    @IBOutlet weak var rightBorderView: UIView!
    
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet weak var rightImageTop: NSLayoutConstraint!
    @IBOutlet weak var rightImageBottom: NSLayoutConstraint!
    @IBOutlet weak var rightImageRight: NSLayoutConstraint!
    @IBOutlet weak var rightImageLeft: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.leftImage.image = nil
        self.rightImage.image = nil
        
        self.bannerImageView.backgroundColor = UIColor.blackColor()
        self.bannerImageView.image = nil
        if let stringUrl = self.banner?.bannerUrl, let url = NSURL(string: stringUrl) {
            self.bannerImageView.af_setImageWithURL(url, imageTransition: UIImageView.ImageTransition.CrossDissolve(1), runImageTransitionIfCached: false)
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let v = templatePosition == .left ? self.leftView : self.rightView
        self.updateMinZoomScaleForSize(v.bounds.size)
    }
    
    @IBAction func shareTap() {
        let img = UIImage(view: self.containerView)
        
        let shareItems:Array = [img]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityTypePrint, UIActivityTypePostToWeibo, UIActivityTypeCopyToPasteboard, UIActivityTypeAddToReadingList, UIActivityTypePostToVimeo]
        self.presentViewController(activityViewController, animated: true, completion: nil)
        
        activityViewController.completionWithItemsHandler = {(activityType, completed:Bool, returnedItems:[AnyObject]?, error: NSError?) in
            if completed {
                self.datasource.logShared(self.banner!.id, completion: { (error) in
                    
                })
            }
        }
    }

    private func setup() {
        self.templatePosition == .left ? self.setupLeft() : self.setupRight()
    }
    
    private func setupLeft() {
        self.rightView.hidden = true
        self.leftPlaceholder.image = self.templateImage
        self.leftView.clipsToBounds = true
        self.leftView.maskView = self.getMask()
        self.leftView.maskView?.clipsToBounds = true
        self.leftScrollView.delegate = self
        
        self.leftBorderView.clipsToBounds = true
        self.leftBorderView.maskView = self.getMask()
        self.leftBorderView.maskView?.clipsToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectImage))
        tap.numberOfTapsRequired = 1
        self.leftScrollView.userInteractionEnabled = true
        self.leftScrollView.addGestureRecognizer(tap)
    }
    
    private func setupRight() {
        self.leftView.hidden = true
        self.rightPlaceholder.image = self.templateImage
        self.rightView.clipsToBounds = true
        self.rightView.maskView = self.getMask()
        self.rightView.maskView?.clipsToBounds = true
        self.rightScrollView.delegate = self
        
        self.rightBorderView.clipsToBounds = true
        self.rightBorderView.maskView = self.getMask()
        self.rightBorderView.maskView?.clipsToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectImage))
        tap.numberOfTapsRequired = 1
        self.rightScrollView.userInteractionEnabled = true
        self.rightScrollView.addGestureRecognizer(tap)
    }
    
    private func getMask() -> UIImageView {
        let imageView = UIImageView(image: self.templateImage)
        
        
        imageView.frame = self.templatePosition == .left ? self.leftPlaceholder.bounds : self.rightPlaceholder.bounds
        return imageView
    }
    
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
    }
    
    private func updateMinZoomScaleForSize(size: CGSize) {
        if templatePosition == .left {
            let widthScale = (size.width / self.leftImage.bounds.width) * 2
            let heightScale = (size.height / self.leftImage.bounds.height)
            let minScale = min(widthScale, heightScale)
            
            self.leftScrollView.minimumZoomScale = minScale
            self.leftScrollView.zoomScale = minScale
        } else {
            let widthScale = (size.width / self.rightImage.bounds.width) * 2
            let heightScale = (size.height / self.rightImage.bounds.height)
            let minScale = min(widthScale, heightScale)
            
            self.rightScrollView.minimumZoomScale = minScale
            self.rightScrollView.zoomScale = minScale
        }
    }
    
    
    private func updateConstraintsForSize(size: CGSize) {
        
        if templatePosition == .left {
            let yOffset = max(0, (size.height - self.leftImage.frame.height) / 2)
            self.leftImageTop.constant = yOffset
            self.leftImageBottom.constant = yOffset

            let xOffset = max(0, (size.width - self.leftImage.frame.width) / 2)
            self.leftImageRight.constant = xOffset
            self.leftImageLeft.constant = xOffset
        } else {
            let yOffset = max(0, (size.height - self.rightImage.frame.height) / 2)
            self.rightImageTop.constant = yOffset
            self.rightImageBottom.constant = yOffset

            let xOffset = max(0, (size.width - self.rightImage.frame.width) / 2)
            self.rightImageRight.constant = xOffset
            self.rightImageLeft.constant = xOffset
        }
        
        view.layoutIfNeeded()
    }
    
}

extension BannerPhotoEditVC: UIScrollViewDelegate {
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        // 1
        return templatePosition == .left ? leftImage : rightImage
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        let v = templatePosition == .left ? self.leftView : self.rightView
        print("facut")
        updateConstraintsForSize(v.bounds.size)  // 4
    }
    
}

extension BannerPhotoEditVC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @objc private func selectImage() {
        let imagePickerController = NonRotationImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum
        //imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera
        //imagePickerController.allowsEditing = true
        
        imagePickerController.view.frame = self.view.frame
        self.addChildViewController(imagePickerController)
        self.view.addSubview(imagePickerController.view)
        imagePickerController.didMoveToParentViewController(self)
        
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        let vc = self.childViewControllers.last
        vc?.view.removeFromSuperview()
        vc?.removeFromParentViewController()
    }

    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        
        let vc = self.childViewControllers.last
        vc?.view.removeFromSuperview()
        vc?.removeFromParentViewController()
        
        if templatePosition == .left {
            self.leftBorderView.hidden = false
            self.leftImage.image = image
            self.leftPlaceholder.hidden = true
            self.updateConstraintsForSize(leftView.bounds.size)
            self.updateMinZoomScaleForSize(leftView.bounds.size)
        } else {
            self.rightBorderView.hidden = false
            self.rightImage.image = image
            self.rightPlaceholder.hidden = true
            self.updateConstraintsForSize(rightView.bounds.size)
            self.updateMinZoomScaleForSize(rightView.bounds.size)
        }
        
        
        
//        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
