//
//  EntitiesVC.swift
//  Godmode
//
//  Created by Andrei Calbajos on 25/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll

class PublisherListVC: UIViewController {
    
    let dataSource = PublisherDataSource()
    
    @IBOutlet weak var entitiesTableView: UITableView!
    var refreshControl:UIRefreshControl!
    
    @IBOutlet weak var GMMTemplatesButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 26.0/255.0, green: 27.0/255.0, blue: 31.0/255.0, alpha: 1)
        self.addPullToRefresh()
        self.addLazyLoading()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        Intro.defaultIntro.show(self)
    }
    
    @IBAction func favoriteTap() {
        self.navigationController?.pushFavoritePublishersVC(self)
    }
    
    @IBAction func searchTap() {
        self.navigationController?.pushSearchPublishersVC(self)
    }
    
    @IBAction func gmmTemplatesTap() {
        let story = UIStoryboard(name: "GMMTemplates", bundle: nil)
        let destination = story.instantiateViewControllerWithIdentifier("GMMTemplatesVC") as! GMMTemplatesVC
        self.navigationController?.pushViewController(destination, animated: true)
    }
    

    
}

extension PublisherListVC {
    private func addPushNotificationHandler() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.handlePushNotification(_:)), name: "openNotification", object: nil)
    }
    
    @objc private func handlePushNotification(notification: NSNotification) {
        if let banner = notification.object as? Banner {
            
            dump(banner)
            
            self.navigationController?.pushBannerTemplatesVC(banner)
        }
    }
}

extension PublisherListVC: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.publishersCount()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellID = "PublisherListCell"
        let cell = entitiesTableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as! PublisherListCell
        
        cell.configureCellWithModel(self.dataSource.publisherAtIndex(indexPath.row))
        
        return cell
    }
}

extension PublisherListVC: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        self.navigationController?.pushPublisherDetailVC((nil, nil), publisher: self.dataSource.publisherAtIndex(indexPath.row))
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (UIScreen.mainScreen().bounds.width * 315) / 851
    }
}






extension PublisherListVC: PublisherDetailDelegate {
    func publisherDetail(favoritedOrUnfavorited publisher: Publisher?) {
        guard self.dataSource.publishers != nil && publisher != nil else { return }
        for (index, item) in self.dataSource.publishers!.enumerate() where item.id! == publisher!.id! {
            dump(item)
            dump(publisher)
            self.dataSource.publishers![index].favorite = publisher!.favorite
            dump(item)
            break
        }
    }
}

extension PublisherListVC {
    func addPullToRefresh() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = UIColor.blackColor()
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.entitiesTableView.addSubview(refreshControl)
        
        self.entitiesTableView.setContentOffset(CGPointMake(0, -self.refreshControl.frame.size.height), animated: true)
        self.refreshControl.beginRefreshing()
        
        self.getData()
    }
    
    func refresh(sender:AnyObject) {
        self.getData()
    }
    
    private func getData() {
        self.dataSource.loadPublishers { (publishers, currentPage, hasMorePages, error) in
            if error == nil {
                self.dataSource.publishers = publishers
                self.entitiesTableView.reloadData()
                self.refreshControl.endRefreshing()
            } else {
                self.entitiesTableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
}

extension PublisherListVC {
    private func addLazyLoading() {
        self.entitiesTableView.infiniteScrollIndicatorStyle = .Gray
        self.entitiesTableView.addInfiniteScrollWithHandler { (tableView) in
            
            guard self.dataSource.hasMorePages else {
                tableView.finishInfiniteScroll()
                return
            }
            
            let page = self.dataSource.currentPage + 1
            
            self.dataSource.loadPublishers(page, completion: { (publishers, currentPage, hasMorePages, error) in
                if error == nil {
                    var currentCount = self.dataSource.publishersCount() - 1
                    var indexPaths = [NSIndexPath]()
                    for item in publishers! {
                        currentCount += 1
                        let indexPath = NSIndexPath(forRow: currentCount, inSection: 0)
                        
                        indexPaths.append(indexPath)
                        self.dataSource.publishers?.append(item)
                    }
                    
                    tableView.beginUpdates()
                    tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
                    tableView.endUpdates()
                    
                }
                tableView.finishInfiniteScroll()
            })
            
        }
        
    }
}

extension PublisherListVC: IntroDelegate {
    func introFinished() {
        self.addPushNotificationHandler()
        AppDelegate.registerForRemoteNotifications()
    }
}

