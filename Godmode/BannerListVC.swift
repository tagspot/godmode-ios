//
//  BannerListVC.swift
//  Godmode
//
//  Created by Andrei Calbajos on 28/07/16.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll

class BannerListVC: UIViewController {
    
    let dataSource = BannersDataSouce()
    
    weak var publisher: Publisher?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bannerListTableView: UITableView!
    var refreshControl:UIRefreshControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.addReloadBannerObserver()

        self.addPullToRefresh()
        self.addLazyLoading()
    }
    
    private func addReloadBannerObserver() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BannerListVC.reloadBanners(_:)), name: "reloadBanners", object: nil)
    }
    
    func reloadBanners(sender: NSNotification) {
        self.navigationController?.pushBannerListVC(self.publisher)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.bannerListTableView.reloadData()
        
    }
    
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
}

extension BannerListVC: UITableViewDataSource {
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellID = "BannerListCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as! BannerListCell
        cell.configureCellWithModel(self.dataSource.bannerAtIndex(indexPath.row))
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.bannersCount()
    }
}

extension BannerListVC: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.navigationController?.pushBannerTemplatesVC(self.dataSource.bannerAtIndex(indexPath.row))
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return (UIScreen.mainScreen().bounds.width * 315) / 851
    }
}

extension BannerListVC {
    func addPullToRefresh() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = UIColor.blackColor()
        self.refreshControl.addTarget(self, action: #selector(self.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        self.bannerListTableView.addSubview(refreshControl)
        
        self.bannerListTableView.setContentOffset(CGPointMake(0, -self.refreshControl.frame.size.height), animated: true)
        self.refreshControl.beginRefreshing()
        
        self.getData()
    }
    
    func refresh(sender:AnyObject) {
        self.getData()
    }
    
    private func getData() {
        self.dataSource.loadBanners(publisherId: self.publisher!.id) { (banners, currentPage, hasMorePages, error) in
            if error == nil {
                self.dataSource.banners = banners
                self.bannerListTableView.reloadData()
                self.refreshControl.endRefreshing()
            } else {
                self.bannerListTableView.reloadData()
                self.refreshControl.endRefreshing()
            }
        }
    }
}

extension BannerListVC {
    private func addLazyLoading() {
        self.bannerListTableView.infiniteScrollIndicatorStyle = .Gray
        self.bannerListTableView.addInfiniteScrollWithHandler { (tableView) in
            
            guard self.dataSource.hasMorePages else {
                tableView.finishInfiniteScroll()
                return
            }
            
            let page = self.dataSource.currentPage + 1
            
            self.dataSource.loadBanners(page, publisherId: self.publisher!.id, completion: { (banners, currentPage, hasMorePages, error) in
                if error == nil {
                    var currentCount = self.dataSource.bannersCount() - 1
                    var indexPaths = [NSIndexPath]()
                    for item in banners! {
                        currentCount += 1
                        let indexPath = NSIndexPath(forRow: currentCount, inSection: 0)
                        
                        indexPaths.append(indexPath)
                        self.dataSource.banners?.append(item)
                    }
                    
                    tableView.beginUpdates()
                    tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Automatic)
                    tableView.endUpdates()
                    
                }
                tableView.finishInfiniteScroll()
            })
            
        }
        
    }
}
