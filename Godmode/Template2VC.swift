//
//  Template2VC.swift
//  Godmode
//
//  Created by Adrian Barbos on 17/09/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

class Template2VC: UIViewController {
    
    var slices = [
        UIImage(named: "21.png"),
        UIImage(named: "22.png"),
        UIImage(named: "23.png"),
        UIImage(named: "24.png")
    ]
    
    var activeScrollView = UIScrollView()
    
    @IBOutlet weak var containerView: UIView!
    
    //MARK: - S 1
    @IBOutlet weak var placeholderS1: UIImageView!
    @IBOutlet weak var S1View: UIView!
    @IBOutlet weak var S1ScrollView: UIScrollView!
    
    @IBOutlet weak var S1Image: UIImageView!
    @IBOutlet weak var S1ImageTop: NSLayoutConstraint!
    @IBOutlet weak var S1ImageBottom: NSLayoutConstraint!
    @IBOutlet weak var S1ImageRight: NSLayoutConstraint!
    @IBOutlet weak var S1ImageLeft: NSLayoutConstraint!
    
    //MARK: - S 2
    @IBOutlet weak var placeholderS2: UIImageView!
    @IBOutlet weak var S2View: UIView!
    @IBOutlet weak var S2ScrollView: UIScrollView!
    
    @IBOutlet weak var S2Image: UIImageView!
    @IBOutlet weak var S2ImageTop: NSLayoutConstraint!
    @IBOutlet weak var S2ImageBottom: NSLayoutConstraint!
    @IBOutlet weak var S2ImageRight: NSLayoutConstraint!
    @IBOutlet weak var S2ImageLeft: NSLayoutConstraint!
    
    //MARK: - S 3
    @IBOutlet weak var placeholderS3: UIImageView!
    @IBOutlet weak var S3View: UIView!
    @IBOutlet weak var S3ScrollView: UIScrollView!
    
    @IBOutlet weak var S3Image: UIImageView!
    @IBOutlet weak var S3ImageTop: NSLayoutConstraint!
    @IBOutlet weak var S3ImageBottom: NSLayoutConstraint!
    @IBOutlet weak var S3ImageRight: NSLayoutConstraint!
    @IBOutlet weak var S3ImageLeft: NSLayoutConstraint!
    
    //MARK: - S 4
    @IBOutlet weak var placeholderS4: UIImageView!
    @IBOutlet weak var S4View: UIView!
    @IBOutlet weak var S4ScrollView: UIScrollView!
    
    @IBOutlet weak var S4Image: UIImageView!
    @IBOutlet weak var S4ImageTop: NSLayoutConstraint!
    @IBOutlet weak var S4ImageBottom: NSLayoutConstraint!
    @IBOutlet weak var S4ImageRight: NSLayoutConstraint!
    @IBOutlet weak var S4ImageLeft: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.placeholderS1.image = self.slices[0]
        self.placeholderS2.image = self.slices[1]
        self.placeholderS3.image = self.slices[2]
        self.placeholderS4.image = self.slices[3]
        
        self.S1Image.image = nil
        self.S2Image.image = nil
        self.S3Image.image = nil
        self.S4Image.image = nil
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let value = UIInterfaceOrientation.LandscapeRight.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        let value = UIInterfaceOrientation.Portrait.rawValue
        UIDevice.currentDevice().setValue(value, forKey: "orientation")
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.updateMinZoomScaleForSize(self.S1View.bounds.size, image: self.S1Image, scroll: self.S1ScrollView)
        self.updateMinZoomScaleForSize(self.S2View.bounds.size, image: self.S2Image, scroll: self.S2ScrollView)
        self.updateMinZoomScaleForSize(self.S3View.bounds.size, image: self.S3Image, scroll: self.S3ScrollView)
        self.updateMinZoomScaleForSize(self.S4View.bounds.size, image: self.S4Image, scroll: self.S4ScrollView)
    }
    
    @IBAction func back() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func shareTap() {
        let img = UIImage(view: self.containerView)
        
        let shareItems:Array = [img]
        let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        activityViewController.excludedActivityTypes = [UIActivityTypePrint, UIActivityTypePostToWeibo, UIActivityTypeCopyToPasteboard, UIActivityTypeAddToReadingList, UIActivityTypePostToVimeo]
        self.presentViewController(activityViewController, animated: true, completion: nil)
        
        activityViewController.completionWithItemsHandler = {(activityType, completed:Bool, returnedItems:[AnyObject]?, error: NSError?) in
            if completed {
                //                self.datasource.logShared(self.banner!.id, completion: { (error) in
                //
                //                })
            }
        }
    }
    
}

extension Template2VC {
    private func setup() {
        self.setupSlice(
            self.S1View,
            placeholder: self.placeholderS1,
            scroll: self.S1ScrollView,
            slice: self.slices[0]!
        )
        
        self.setupSlice(
            self.S2View,
            placeholder: self.placeholderS2,
            scroll: self.S2ScrollView,
            slice: self.slices[1]!
        )
        
        self.setupSlice(
            self.S3View,
            placeholder: self.placeholderS3,
            scroll: self.S3ScrollView,
            slice: self.slices[2]!
        )
        
        self.setupSlice(
            self.S4View,
            placeholder: self.placeholderS4,
            scroll: self.S4ScrollView,
            slice: self.slices[3]!
        )
    }
    
    private func setupSlice(view: UIView, placeholder: UIImageView, scroll: UIScrollView, slice: UIImage) {
        let imageView = UIImageView(image: slice)
        imageView.frame = placeholder.bounds
        
        view.clipsToBounds = true
        view.maskView = imageView
        view.maskView?.clipsToBounds = true
        scroll.delegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.selectImage))
        tap.numberOfTapsRequired = 1
        scroll.userInteractionEnabled = true
        scroll.addGestureRecognizer(tap)
    }
    
    private func updateMinZoomScaleForSize(size: CGSize, image: UIImageView, scroll: UIScrollView) {
        
        let widthScale = (size.width / image.bounds.width)
        let heightScale = (size.height / image.bounds.height)
        let minScale = max(widthScale, heightScale)
        
        scroll.minimumZoomScale = minScale
        scroll.zoomScale = minScale
    }
    
    
    private func updateConstraintsForSize(size: CGSize, image: UIImageView, top: NSLayoutConstraint, bottom: NSLayoutConstraint, left: NSLayoutConstraint, right: NSLayoutConstraint) {
        
        let yOffset = max(0, (size.height - image.frame.height) / 2)
        top.constant = yOffset
        bottom.constant = yOffset
        
        let xOffset = max(0, (size.width - image.frame.width) / 2)
        right.constant = xOffset
        left.constant = xOffset
        
        view.layoutIfNeeded()
    }
}

extension Template2VC: UIScrollViewDelegate {
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        switch scrollView {
        case self.S1ScrollView:
            return self.S1Image
            
        case self.S2ScrollView:
            return self.S2Image
            
        case self.S3ScrollView:
            return self.S3Image
            
        case self.S4ScrollView:
            return self.S4Image
            
        default:
            return nil
        }
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        
        
        switch scrollView {
        case self.S1ScrollView:
            self.updateConstraintsForSize(
                self.S1View.bounds.size,
                image: self.S1Image,
                top: self.S1ImageTop,
                bottom: self.S1ImageBottom,
                left: self.S1ImageLeft,
                right: self.S1ImageRight
            )
            break
            
        case self.S2ScrollView:
            self.updateConstraintsForSize(
                self.S2View.bounds.size,
                image: self.S2Image,
                top: self.S2ImageTop,
                bottom: self.S2ImageBottom,
                left: self.S2ImageLeft,
                right: self.S2ImageRight
            )
            break
            
        case self.S3ScrollView:
            self.updateConstraintsForSize(
                self.S3View.bounds.size,
                image: self.S3Image,
                top: self.S3ImageTop,
                bottom: self.S3ImageBottom,
                left: self.S3ImageLeft,
                right: self.S3ImageRight
            )
            break
            
        case self.S4ScrollView:
            self.updateConstraintsForSize(
                self.S4View.bounds.size,
                image: self.S4Image,
                top: self.S4ImageTop,
                bottom: self.S4ImageBottom,
                left: self.S4ImageLeft,
                right: self.S4ImageRight
            )
            break
            
        default:
            break
        }
    }
    
}

extension Template2VC: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @objc private func selectImage(sender: UITapGestureRecognizer) {
        
        self.activeScrollView = sender.view as! UIScrollView
        
        let imagePickerController = NonRotationImagePickerController()
        imagePickerController.delegate = self
        imagePickerController.sourceType = UIImagePickerControllerSourceType.SavedPhotosAlbum

        imagePickerController.view.frame = self.view.frame
        self.addChildViewController(imagePickerController)
        self.view.addSubview(imagePickerController.view)
        imagePickerController.didMoveToParentViewController(self)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        let vc = self.childViewControllers.last
        vc?.view.removeFromSuperview()
        vc?.removeFromParentViewController()
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        
        let vc = self.childViewControllers.last
        vc?.view.removeFromSuperview()
        vc?.removeFromParentViewController()

        switch self.activeScrollView {
        case self.S1ScrollView:
            self.S1Image.image = image
            self.updateConstraintsForSize(self.S1View.bounds.size, image: self.S1Image, top: self.S1ImageTop, bottom: self.S1ImageBottom, left: self.S1ImageLeft, right: self.S1ImageRight)
            self.updateMinZoomScaleForSize(self.S1View.bounds.size, image: self.S1Image, scroll: self.S1ScrollView)
            break
            
        case self.S2ScrollView:
            self.S2Image.image = image
            self.updateConstraintsForSize(self.S2View.bounds.size, image: self.S2Image, top: self.S2ImageTop, bottom: self.S2ImageBottom, left: self.S2ImageLeft, right: self.S2ImageRight)
            self.updateMinZoomScaleForSize(self.S2View.bounds.size, image: self.S2Image, scroll: self.S2ScrollView)
            break
            
        case self.S3ScrollView:
            self.S3Image.image = image
            self.updateConstraintsForSize(self.S3View.bounds.size, image: self.S3Image, top: self.S3ImageTop, bottom: self.S3ImageBottom, left: self.S3ImageLeft, right: self.S3ImageRight)
            self.updateMinZoomScaleForSize(self.S3View.bounds.size, image: self.S3Image, scroll: self.S3ScrollView)
            break
            
        case self.S4ScrollView:
            self.S4Image.image = image
            self.updateConstraintsForSize(self.S4View.bounds.size, image: self.S4Image, top: self.S4ImageTop, bottom: self.S4ImageBottom, left: self.S4ImageLeft, right: self.S4ImageRight)
            self.updateMinZoomScaleForSize(self.S4View.bounds.size, image: self.S4Image, scroll: self.S4ScrollView)
            break
            
        default:
            break
        }
        
    }
}