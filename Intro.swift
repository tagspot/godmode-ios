//
//  Intro.swift
//  Godmode
//
//  Created by Adrian Barbos on 16/09/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import EAIntroView

protocol IntroDelegate: class {
    func introFinished()
}

class Intro: NSObject {
    
    static let defaultIntro = Intro()
    
    private var sender = UIViewController()
    private var delegate: IntroDelegate?
    
    private var introWasShown: Bool {
        get {
            return NSUserDefaults.standardUserDefaults().boolForKey("introWasShown") ?? false
        } set {
            NSUserDefaults.standardUserDefaults().setObject(newValue, forKey: "introWasShown")
        }
    }
    
    internal func show(sender: PublisherListVC) {
        
        guard !self.introWasShown else { return }
        
        self.sender = sender
        self.delegate = sender
        
        self.initIntroView()
        
    }
    
    private func initIntroView() {
        let intro = EAIntroView(frame: self.sender.view.bounds, andPages: self.getPages())
        intro.delegate = self
        
        intro.skipButton = nil
        intro.skipButtonAlignment = .Center
        
        intro.showInView(sender.view)
    }
    
    private func getPages() -> [EAIntroPage] {
        let page1 = EAIntroPage()
        page1.bgImage = UIImage(named: "tut-mob-1")
        page1.title = "Find your favorite artists and discover new events"
        
        let page2 = EAIntroPage()
        page2.bgImage = UIImage(named: "tut-mob-2")
        page2.title = "Select an image and create your own custom banners"
        
        let page3 = EAIntroPage()
        page3.bgImage = UIImage(named: "tut-mob-3")
        page3.title = "Share it with the world"
        
        return [page1, page2, page3]
    }
    
}

extension Intro: EAIntroDelegate {
    func introDidFinish(introView: EAIntroView!, wasSkipped: Bool) {
        self.introWasShown = true
        self.delegate?.introFinished()
    }
    
    func intro(introView: EAIntroView!, pageStartScrolling page: EAIntroPage!, withIndex pageIndex: UInt) {
        introView.pageControl.hidden = true
        introView.skipButton = nil
    }
    
    func intro(introView: EAIntroView!, pageEndScrolling page: EAIntroPage!, withIndex pageIndex: UInt) {
        let button = UIButton()
        button.frame.size = CGSize(
            width: UIScreen.mainScreen().bounds.size.width * 0.7,
            height: 45
        )
        button.setTitle("Get started", forState: .Normal)
        button.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        button.layer.cornerRadius = 15
        button.layer.borderColor = UIColor.whiteColor().CGColor
        button.layer.borderWidth = 1
        button.backgroundColor = UIColor.clearColor()
        
        switch pageIndex {
        case 2:
            introView.pageControl.hidden = true
            introView.skipButton = button
            break
            
        default:
            introView.pageControl.hidden = false
            introView.skipButton = nil
            break
        }
    }
}