//
//  UIImagePickerExtension.swift
//  Godmode
//
//  Created by Adrian Barbos on 16/09/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

class NonRotationImagePickerController: UIImagePickerController {
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.LandscapeRight
    }
    
}