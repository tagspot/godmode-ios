//
//  BannersAPI.swift
//  Godmode
//
//  Created by Adrian Barbos on 31/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation

extension APIClient {
    
    func banners(page: Int, publisherId: Int?, completion: APICompletionBlock) {
        
        let serverPath = "/publisher/\(publisherId ?? 0)/banner?page=\(page)"
        
        self.dataTask(
            .GET,
            serverPath: serverPath,
            params: nil,
            headers: nil,
            completion: completion
        )
    }
    
    func logSharedBanner(bannerId: Int?, completion: APICompletionBlock) {
        
        let serverPath = "/banner/\(bannerId ?? 0)/share"
        
        self.dataTask(
            .POST,
            serverPath: serverPath,
            params: nil,
            headers: nil,
            completion: completion
        )
    }
    
}