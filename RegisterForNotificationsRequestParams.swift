//
//  RegisterForNotificationsRequestParams.swift
//  Godmode
//
//  Created by Adrian Barbos on 09/08/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation

class RegisterForNotificationsRequestParams {
    
    var token: String?
    let type: String = "ios"
    
}