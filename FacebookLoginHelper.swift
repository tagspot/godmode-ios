//
//  FacebookDataSource.swift
//  Godmode
//
//  Created by Adrian Barbos on 30/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit
import FBSDKLoginKit

typealias FacebookCompletionBlock = ((status: FacebookStatus, token: String?) -> ())

enum FacebookStatus {
    case canceled
    case success
    case error
}

class FacebookLoginHelper {
    
    private init() {}
    
    static let sharedInstance = FacebookLoginHelper()
    
    func login(sender: UIViewController, completion: FacebookCompletionBlock) {
        
        if FBSDKAccessToken.currentAccessToken() != nil {
            FBSDKLoginManager().logOut()
        }
        
        let permissions = ["public_profile", "email"]
        
        let login = FBSDKLoginManager()
        login.logInWithReadPermissions(permissions, fromViewController: sender) { (result, error) -> Void in
            if (error != nil) {
                completion(status: FacebookStatus.error, token: nil)
            } else if result.isCancelled {
                completion(status: FacebookStatus.canceled, token: nil)
            } else {
                completion(status: FacebookStatus.success, token: FBSDKAccessToken.currentAccessToken().tokenString)
            }
        }
    }
    
    
}