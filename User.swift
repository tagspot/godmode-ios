//
//  User.swift
//  Godmode
//
//  Created by Adrian Barbos on 25/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import UIKit

class User {
    
    /**
     * Init singleton
    **/
    private init() {}
    static let sharedInstance = User()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    let notificationCenter = NSNotificationCenter.defaultCenter()
    
    internal var token: String? {
        get {
            return self.defaults.stringForKey("userToken")
        } set {
            self.defaults.setObject(newValue, forKey: "userToken")
        }
    }
    
    internal var isLoggedIn: Bool {
        return self.token != nil
    }
    
    internal func login(sender: UIViewController, token: String?) {
        self.token = token
        sender.navigationController?.popToRootViewControllerAnimated(false)
        self.notificationCenter.postNotificationName("mainLogin", object: nil)
    }
    
    internal func logout(sender: UIViewController) {
        self.token = nil
        sender.navigationController?.popToRootViewControllerAnimated(false)
        self.notificationCenter.postNotificationName("mainLogout", object: nil)
    }
    
}