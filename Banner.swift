//
//  Banner.swift
//  Godmode
//
//  Created by Adrian Barbos on 31/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation
import LBJSON

class Banner {
    
    var id: Int?
    var bannerUrl: String?
    
    init(item: LBJSON) {
        self.id = item["id"]?.int
        self.bannerUrl = item["banner_url"]?.str
    }
    
}