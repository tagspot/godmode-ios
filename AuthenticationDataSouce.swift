//
//  AuthenticationDataSouce.swift
//  Godmode
//
//  Created by Adrian Barbos on 29/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation

class AuthenticationDataSource {
    
    func login(params: SignInRequestParams, completion: SignInCompletionBlock) {
        self.loginMethod(params, completion: completion)
    }
    
    func register(params: SignUpRequestParams, completion: SignUpCompletionBlock) {
        self.registerMethod(params, completion: completion)
    }
    
    func facebookLogin(token: String?, completion: FacebookLoginCompletionBlock) {
        self.facebookLoginMethod(token, completion: completion)
    }
    
}

extension AuthenticationDataSource {
    
    private func loginMethod(params: SignInRequestParams, completion: SignInCompletionBlock) {
        APIClient.shareInstance().signIn(params) { (response, error, statusCode) in
            if error == nil {
                AuthenticationParser.sharedInstance.parseSignIn(response, completion: completion)
            } else if let statusCode = statusCode where statusCode == 401 {
                completion(token: nil, status: SignInStatus.wrongPassword)
            } else {
                completion(token: nil, status: SignInStatus.networkError)
            }
        }
    }
    
    private func registerMethod(params: SignUpRequestParams, completion: SignUpCompletionBlock) {
        APIClient.shareInstance().signUp(params) { (response, error, statusCode) in
            if error == nil {
                AuthenticationParser.sharedInstance.parseSignUp(response, completion: completion)
            } else if let statusCode = statusCode where statusCode == 422 {
                completion(token: nil, status: SignUpStatus.emailExists)
            } else {
                completion(token: nil, status: SignUpStatus.networkError)
            }
        }
    }
    
    private func facebookLoginMethod(token: String?, completion: FacebookLoginCompletionBlock) {
        APIClient.shareInstance().facebookLogin(token) { (response, error, statusCode) in
            if error == nil {
                AuthenticationParser.sharedInstance.parseFacebookLogin(response, completion: completion)
            } else {
                completion(token: nil, error: error)
            }
        }
    }
    
}