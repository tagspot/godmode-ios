//
//  APIClient.swift
//  starcard
//
//  Created by Adrian Barbos on 3/25/16.
//  Copyright © 2016 Banca Transilvania. All rights reserved.
//

import Foundation
import Alamofire

//Enable or disable response and requests logs
let DEBUD_REQUEST_ENABLED = true
let DEBUD_RESPONSE_ENABLED = true

typealias APICompletionBlock = (response:AnyObject?, error:NSError?, statusCode: Int?) -> ()
typealias StatusCompletionBlock = (error: NSError?) -> ()

/*
 *
 *   MARK: - CORE
 *
 */

public final class APIClient {
    
    internal var request: Request?
    
    /**
     **  Base URL
     **/
    internal let baseUrl = "http://godmode-app.com/api"
    internal let version = "v1"
    
    /**
     **  Api URL
     **/
    internal func APIUrl(serverPath: String) -> String { return "\(self.baseUrl)/\(self.version)\(serverPath)" }
    
    /*
     *  MARK: Init singleton instance
     */
    private init(){}
    
    class func shareInstance() -> APIClient {
        struct Singleton {
            static var instance: APIClient? = nil
            static var token:dispatch_once_t = 0
        }
        
        dispatch_once(&Singleton.token, {
            Singleton.instance = APIClient()
        })
        
        return Singleton.instance!
    }
    
    /*
     *  MARK: Main data task method
     *
     *  @return Request
     */
    internal func dataTask(method: Alamofire.Method, serverPath: String, params: [String:AnyObject]?, headers: [String:AnyObject]?, completion: APICompletionBlock) {
        
        //Instantiate parameters array
        var parameters = [String:AnyObject]()
        if let par = params { for item in par { parameters[item.0] = item.1 } }
        
        //Add requiered headers
        var allHeaders = [
            "Authorization" : "Bearer \((User.sharedInstance.token ?? ""))"
        ]
        if let hed = headers { for item in hed { allHeaders[item.0] = "\(item.1)" } }
        
        self.showRequestParams(self.APIUrl(serverPath), parameters: parameters)
        
        self.request = Alamofire.request(method, self.APIUrl(serverPath), parameters: parameters, headers: allHeaders)
        
        self.request!
            .validate(statusCode: 200..<300)
            .responseJSON { (responseObject) in
                
                let statusCode = responseObject.response?.statusCode
                
                switch responseObject.result {
                case .Success:
                    if let response = responseObject.result.value {
                        self.showResponse(serverPath, response: response)
                        completion(response: response, error: nil, statusCode: statusCode)
                    } else {
                        self.showResponse(serverPath, response: responseObject.result.error ?? "Cant get error data")
                        completion(response: nil, error: responseObject.result.error, statusCode: statusCode)
                    }
                    break
                case .Failure(let error):
                    if let response = responseObject.result.value {
                        self.showResponse(serverPath, response: response)
                        completion(response: response, error: nil, statusCode: statusCode)
                    } else {
                        self.showResponse(serverPath, response: error ?? "Cant get error data")
                        completion(response: nil, error: responseObject.result.error, statusCode: statusCode)
                    }
                    break
                }
                
        }
        
    }
    
    func cancelAllRequests() {
        if let request = self.request {
            request.cancel()
        }
    }
    
    internal func serializeDictionary(dict:AnyObject) -> String? {
        if let jsonData = try? NSJSONSerialization.dataWithJSONObject(dict, options: []) {
            let str = String(data: jsonData, encoding: NSUTF8StringEncoding)
            return str
        }
        return nil
    }
    
    internal func deserializeDictionary(dict: String) -> AnyObject! {
        if let dataString = dict.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false) {
            if let jsonDict = try? NSJSONSerialization.JSONObjectWithData(dataString, options: .AllowFragments) {
                return jsonDict
            }
        }
        return nil
    }
    
}

/*
 *
 *   MARK: - DEBUGGING FUNCTIONS
 *
 */

extension APIClient {
    internal func showRequestParams(method: String, parameters: [String:AnyObject]) {
        guard DEBUD_REQUEST_ENABLED else { return }
        print("")
        print("##########################################")
        print("STARTING REQUEST FOR METHOD | \(method) | WITH PARAMETERS")
        print("##########################################")
        print("")
        print("\(parameters)")
        print("")
        print("##########################################")
        print("")
    }
    
    internal func showResponse(method: String, response: AnyObject) {
        guard DEBUD_RESPONSE_ENABLED else { return }
        print("")
        print("##########################################")
        print("RESPONSE FOR METHOD | \(method) |")
        print("##########################################")
        print("")
        print("\(response)")
        print("")
        print("##########################################")
        print("")
    }
}