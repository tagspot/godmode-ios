//
//  PublisherDataSource.swift
//  Godmode
//
//  Created by Adrian Barbos on 30/07/2016.
//  Copyright © 2016 Andrei Calbajos. All rights reserved.
//

import Foundation

class PublisherDataSource {
    
    var publishers: [Publisher]?
    var currentPage: Int = 1
    var hasMorePages = false
    
    func loadPublishers(page: Int = 1, search: String? = nil, favorite: Bool = false, completion: PublishersCompletionBlock) {
        self.loadPublishersMethod(page, search: search, favorite: favorite, completion: completion)
    }
    
    func favoritePublisher(publisherId: Int?, completion: StatusCompletionBlock) {
        self.favoritePublisherMethod(publisherId, completion: completion)
    }
    
    func unfavoritePublisher(publisherId: Int?, completion: StatusCompletionBlock) {
        self.unfavoritePublisherMethod(publisherId, completion: completion)
    }
    
}

extension PublisherDataSource {
    
    func publisherAtIndex(index: Int) -> Publisher? {
        if let objects = self.publishers {
            if index < 0 || index >= objects.count {
                return nil
            } else {
                return objects[index]
            }
        }
        return nil
    }
    
    func publishersCount() -> Int {
        return self.publishers?.count ?? 0
    }
    
}

extension PublisherDataSource {
    
    private func loadPublishersMethod(page: Int = 1, search: String? = nil, favorite: Bool = false, completion: PublishersCompletionBlock) {
        APIClient.shareInstance().publishers(page, search: search, favorite: favorite) { (response, error, statusCode) in
            if error == nil {
                PublisherParser.sharedInstance.parsePublishers(response, completion: { (publishers, currentPage, hasMorePages, error) in
                    if error == nil {
                        self.currentPage = currentPage!
                        self.hasMorePages = hasMorePages!
                        completion(publishers: publishers, currentPage: currentPage, hasMorePages: hasMorePages, error: nil)
                    } else {
                        completion(publishers: nil, currentPage: nil, hasMorePages: nil, error: error)
                    }
                })
            } else {
                completion(publishers: nil, currentPage: nil, hasMorePages: nil, error: error)
            }
        }
    }
    
    private func favoritePublisherMethod(publisherId: Int?, completion: StatusCompletionBlock) {
        APIClient.shareInstance().favoritePublisher(publisherId) { (response, error, statusCode) in
            completion(error: error)
        }
    }
    
    private func unfavoritePublisherMethod(publisherId: Int?, completion: StatusCompletionBlock) {
        APIClient.shareInstance().unfavoritePublisher(publisherId) { (response, error, statusCode) in
            completion(error: error)
        }
    }
    
}